<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\NoteRequest;
use App\Http\Resources\Api\NotesResource;
use App\Models\Note;
use App\Repositories\NoteRepository;
use App\Services\NoteService;
use App\Traits\API;
use Illuminate\Http\Request;

class NotesController extends Controller
{
    protected $noteService;

    public function __construct()
    {
        $this->noteService = new NoteService(new NoteRepository());
    }

    public function index(Request $request)
    {
        try {
            $perPage = $request->has('per_page') ? intval($request->input('per_page')) : 6;
            $notes = $this->noteService->getAll(auth()->user());
            return (new API)
                ->isOk(__('Notes.'))
                ->setData($perPage ? api_model_set_paginate(NotesResource::collection($notes) ,$notes) : NotesResource::collection($notes))
                ->build();
        } catch (\Exception $e) {
            return (new API)
                ->isError(__('sorry please try later.'))
                ->build();
        }

    }

    public function store(NoteRequest $request)
    {
        try {
        $data = $request->validated();
        $data['user_id'] = auth()->user()->id;
        $note = $this->noteService->create($data);
        return (new API)
            ->isOk(__('Note created Successful.'))
            ->setData(NotesResource::make($note))
            ->build();
         } catch (\Exception $e) {
              return (new API)
               ->isError(__('sorry please try later.'))
               ->build();
         }
    }

    public function update(NoteRequest $request , Note $note)
    {
        try {
        $data = $request->validated();
        $data['user_id'] = auth()->user()->id;
        $note = $this->noteService->update($note,$data);
        return (new API)
            ->isOk(__('Note updated Successful.'))
            ->setData(NotesResource::make($note))
            ->build();
            } catch (\Exception $e) {
               return (new API)
                ->isError(__('sorry please try later.'))
                ->build();
            }
    }

    public function show(Note $note)
    {
        try {
            return (new API)
                ->isOk(__('Note Data.'))
                ->setData(NotesResource::make($note))
                ->build();
        } catch (\Exception $e) {
            return (new API)
                ->isError(__('sorry please try later.'))
                ->build();
        }

    }

    public function destroy(Note $note)
    {
        try {
            $this->noteService->delete($note);
            return (new API)
                ->isOk(__('Note deleted Successful.'))
                ->build();
        } catch (\Exception $e) {
            return (new API)
                ->isError(__('sorry please try later.'))
                ->build();
        }

    }
}
