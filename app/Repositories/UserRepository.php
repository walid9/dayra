<?php
namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserRepository
{
    public function create($request)
    {
        return User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'password' => $request['password'],
        ]);
    }

    public function login()
    {
        $user = Auth::user();
        $token =  $user->createToken('dayraSanctumAuth')->plainTextToken;
        return $token;
    }
}
