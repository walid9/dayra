<?php
namespace App\Repositories;

use App\Models\Note;

class NoteRepository
{
    public function getAllNotes($user)
    {
        return $user->notes()->select(['id','title' , 'content'])->orderBy('id' , 'desc')->paginate(10);
    }
    public function createNote($data)
    {
        $note = Note::create($data);
        return $note;
    }

    public function updateNote($note ,$data)
    {
        return $note->update($data);
    }

    public function deleteNote($note)
    {
        return $note->delete();
    }
}
