<?php
namespace App\Services;

use App\Repositories\NoteRepository;

class NoteService
{
    protected $noteRepository;


    public function __construct(NoteRepository $noteRepository)
    {
        $this->noteRepository = $noteRepository;
    }

    public function getAll($user)
    {
        return $this->noteRepository->getAllNotes($user);
    }

    public function create($data)
    {
        return $this->noteRepository->createNote($data);
    }

    public function update($note , $data)
    {
        return $this->noteRepository->updateNote($note , $data);
    }

    public function delete($note)
    {
        return $this->noteRepository->deleteNote($note);
    }
}
